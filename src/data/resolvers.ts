import { FortuneCookie } from './connectors';
import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import * as DataLoader from 'dataloader';

const resolvers = {
  Query: {
    getFortuneCookie() {
      return FortuneCookie.getOne();
    }
  }
};

export default resolvers;