import { makeExecutableSchema } from 'graphql-tools';
import mocks from './mocks';
import resolvers from './resolvers';

const typeDefs = `
type Query {
  getFortuneCookie: String
}
`;

const schema = makeExecutableSchema({ typeDefs });

export default makeExecutableSchema({ typeDefs, resolvers });