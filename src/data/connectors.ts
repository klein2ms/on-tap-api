import * as Sequelize from 'sequelize';
import * as Mongoose from 'mongoose';
import * as casual from 'casual';
import * as _ from 'lodash';
import * as fetch from 'node-fetch';

const FortuneCookie = {
  getOne() {
    return fetch('http://fortunecookieapi.herokuapp.com/v1/cookie')
      .then(res => res.json())
      .then(res => {
        return res[0].fortune.message;
      });
  }
};

export { FortuneCookie };


